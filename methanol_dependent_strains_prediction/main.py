# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 17:16:38 2020

@author: kellerp
"""

import cobra
from optslope import calculate_slope_multi, filter_redundant_knockouts, calculate_slope
import pandas as pd
from cobra.io import read_sbml_model


#----------------------------------------------------------------------------------------------------------------

def load_models():
    model = read_sbml_model("core_model_with_rump_frmA_fdh.xml")
    bio_model = model.copy() #biomass function is objective
    r5p_model = model.copy() #r5p production is objective
    r5p_model = set_r5p_model_objective(r5p_model)
    
    return bio_model, r5p_model
    
def set_r5p_model_objective(model):
    objective = cobra.core.Reaction("biomass_precursor", lower_bound=0, upper_bound=1000)
    model.add_reaction(objective)
    objective.add_metabolites({"r5p_c": -1})
    model.objective = objective
    
    return model

def calculate_ratio_ps_methanol(model, target_reaction):
    ratio = calculate_slope(model,[],["methanol"],target_reaction)
    ratio = round(ratio, 3)
    
    return ratio

def calculate_bio(model, carbon_sources, single_kos, target_reaction, max_kos):
    dfs = []
    for carbon_source in carbon_sources:
        df = calculate_slope_multi(model, carbon_source, single_kos, target_reaction, max_kos, num_processes = 10, chunksize = 10)
        dfs.append(df)
    result_df = pd.concat(dfs)
    result_df.carbon_sources = result_df.carbon_sources.str.join(' + ')
    result_df = result_df.round(3).fillna(-1.0)
    summary_df = filter_redundant_knockouts(result_df)
    col_order = list(map(" + ".join, carbon_sources)) + [
        "smallest_slope", "highest_slope", "slope_ratio", "no_knockouts"]
    summary_df = summary_df[col_order]
    summary_df.index = summary_df.index.str.join("|")
        #remove all solutions that are not able to grow on methanol as sole carbon source
    summary_df.round(3).to_excel(excel_writer = "dependent_strains_unfiltered" + str(max_kos) + "_kos.xlsx")
    summary_df = summary_df[summary_df["methanol"] != -1.0]
    
    return result_df, summary_df
    
def normalize_bio(df, bio_ps_methanol):
    #remove all solutions that can produce less biomass from methanol as sole carbon source
    df = df[df["methanol"] <= bio_ps_methanol]
    
    df = df.drop(columns = ["smallest_slope","highest_slope","slope_ratio"])
    for row in df.index:
        if not row:
            df = df.drop(row, axis=0)
        row_str = str(row)
        if row_str == "nan":
            #drop the parental strain row as it is not relevant for the plot
            df = df.drop(row, axis=0)
        
    #normalize the data to the slope on methanol alone
    no_kos = df["no_knockouts"]
    df = df.drop(columns = ["no_knockouts"])
    df = df.div(bio_ps_methanol)
    df = pd.concat([df,no_kos],axis=1)
    
    return df
    
def calculate_r5p(df):
    for column in df.columns:
        cs = str(column).split(" + ")
        if "methanol" in column:
            for index, row in df.iterrows():
                df.at[index,column] = calculate_slope(
                r5p_model,
                [index],
                cs,
                target_reaction
                )
    df[df < 0] = 0
    
    return df  

def normalize_r5p(df, r5p_ps_methanol):
    no_kos = df["no_knockouts"]
    df = df.drop(columns = ["no_knockouts"])
    df = df.div(r5p_ps_methanol)
    df = pd.concat([df,no_kos],axis=1)
    
    return df

def calculate_r5p_bio_Ratio(df, df2):
    ratio_df = df
    for column in df.columns:
        if column != "no_knockouts":
            for index in df[column].index:
                ratio_df[column][index] = df2[column][index]/df[column][index]
                
    return ratio_df

#single kos
single_kos = (
    ["FBP",
     "TPI",
     "FBA",
     "RPI",
     "EDD|EDA",
     "RPE",
     "TALA",
     "TKT1",
     "TKT2",
     "SUCDi",
     "AKGDH",
     "ICL",
     "ME1|ME2",
     "PYK",
     "PGK",
     "PGM",
     "ENO",
     "GAPD",
     "PFK",
     "PGI",
     "PGL",
     "GND",
     "PPS",
     "PPCK",
     "FUM",
     "PDH",
     "MALS",
     "PFL",
     "MDH",
     "SUCOAS",
     "GLUDy",
     "NADTRHD",
     "FRMA"
    ])

#define carbon sources
carbon_sources = (
    ["methanol"],
    ["6pgc","methanol"],
    ["glc__D","methanol"],
    ["r5p","methanol"],
    ["xu5p__D","methanol"],
    ["2pg","methanol"],
    ["ac","methanol"],
    ["pyr","methanol"],
    ["succ","methanol"],
    ["glu__L","methanol"],
    ["fru","methanol"],
    ["dhap","methanol"]
    )

#define reaction that represents methanol assimilation
target_reaction = "H6PS"

#define the maximal number of knockouts
max_kos = 5

#load metabolic models (bio_model with biomass function as objective, r5p_model with r5p production as objective)
bio_model, r5p_model = load_models()

bio_ps_methanol = calculate_ratio_ps_methanol(bio_model, target_reaction)
r5p_ps_methanol = calculate_ratio_ps_methanol(r5p_model, target_reaction)


bio_table_unfiltered, bio_table_filtered = calculate_bio(bio_model, carbon_sources, single_kos, target_reaction, max_kos)
bio_table_filtered_normalized = normalize_bio(bio_table_filtered, bio_ps_methanol)
bio_table_filtered_normalized.round(3).to_excel(excel_writer = "dependent_strains_biomass_fraction_filtered" + str(max_kos) + "_kos.xlsx")

r5p_table_filtered = bio_table_filtered_normalized.copy()
r5p_table_filtered = calculate_r5p(r5p_table_filtered)
r5p_table_filtered_normalized = normalize_r5p(r5p_table_filtered, r5p_ps_methanol)
r5p_table_filtered_normalized.round(3).to_excel(excel_writer = "dependent_strains_R5P_fraction_filtered" + str(max_kos) + "_kos.xlsx")

r5p_bio_Ratio_table = calculate_r5p_bio_Ratio(bio_table_filtered_normalized, r5p_table_filtered_normalized)
r5p_bio_Ratio_table.round(3).fillna(0.0).to_excel(excel_writer = "dependent_strains_R5P_to_biomass_fraction_ratio" + str(max_kos) + "_kos.xlsx")

