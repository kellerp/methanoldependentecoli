# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:37:56 2020

@author: kellerp
"""

import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import (AutoMinorLocator)

plt.rcParams["font.family"] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

data = pd.read_excel("fbp_tpiA_isotopologue_distribution.xlsx")

def plot_bar(data_isotopologue, bottom):
    width = 0.8
    color_list = ["#E94344","#C8DDF3","#8186C1","#204B9B","#272D68","#5F5F5F","#000000"]
    plt.bar(data_isotopologue["metabolite"],
        data_isotopologue["isotopologue fraction"],
        width,
        bottom=bottom,
        color=color_list[data_isotopologue["isotopologue"].values[0]],
        yerr=data_isotopologue["std"]
        )
    plt.gca().spines["top"].set_visible(False)
    plt.gca().spines["right"].set_visible(False)

def plot_fractional_labeling(data, metabolite_list, strain):    
    
    plt.subplots(figsize=(2.75,2.5))
    
    for metabolite in metabolite_list:
        subdata = data[data["metabolite"] == metabolite]
        isotopologue_list=[]
        for isotopologue in subdata["isotopologue"]:
            data_isotopologue = subdata[subdata["isotopologue"] == isotopologue]
            if isotopologue == 0:
                bottom=0
            else:
                bottom = 0
                for iso in isotopologue_list:
                    bottom = bottom + subdata[subdata["isotopologue"] == iso].iloc[0]["isotopologue fraction"]
            isotopologue_list.append(isotopologue)
            plot_bar(data_isotopologue, bottom)
    # Add some text for labels, title and custom x-axis tick labels, etc.
    plt.ylabel('isotopologue fraction')
    plt.xticks(metabolite_list, rotation = "vertical",ha="center")
    plt.ylim(0.0, 1.1)
    plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0])
    plt.tight_layout()
    plt.savefig("isotopologue_distribution_" + strain + ".pdf")
    plt.show()
    

metabolite_list = []
strain_list = []

for metabolite in data["metabolite"]:
    if metabolite not in metabolite_list:
        metabolite_list.append(metabolite)
        
for strain in data["strain"]:
    if strain not in strain_list:
        strain_list.append(strain)
        
for strain in strain_list:
    plot_fractional_labeling(data[data["strain"] == strain], metabolite_list, strain)

