# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:37:56 2020

@author: kellerp
"""

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams["font.family"] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

data = pd.read_excel("fbp_tpiA_fractional_labeling_ancestral_evolved.xlsx")
data_replicates = pd.read_excel("fbp_tpiA_fractional_labeling_ancestral_evolved_replicates.xlsx")

def plot_fractional_labeling(data, data_replicates, metabolite_list, strain):
    data_predicted = data[data["category"] == "predicted"]
    data_ancestral = data[data["category"] == "ancestral"]
    data_evolved = data[data["category"] == "evolved"]
    
    x = np.arange(len(metabolite_list))  # the label locations
    width = 0.3  # the width of the bars
    
    fig, ax = plt.subplots(figsize=(2.75,2.5))
    ax.bar(x - width,
           data_predicted["average"],
           width,
           label='predicted',
           yerr=data_predicted["std"],
           color=("black")
           )
    ax.bar(x,
           data_ancestral["average"],
           width,
           label='ancestral',
           yerr=data_ancestral["std"],
           capsize=3.0,
           color=("#E0A6C0"),
           error_kw=dict(capthick=1)
           )
    ax.bar(x + width,
           data_evolved["average"],
           width,
           label='evolved',
           yerr=data_evolved["std"],
           capsize=3.0,
           color=("#A14A96"),
           error_kw=dict(capthick=1)
           )
    
   
    j=0
    for metabolite in metabolite_list:
        for replicate in data_replicates[data_replicates["metabolite"] == metabolite].iterrows():
            if replicate[1]["category"] == "ancestral":
                ax.scatter(x[j],
                           y = replicate[1]["value"],
                           s=10,
                           facecolors="none",
                           edgecolors="black",
                           linewidths=1.0,
                           zorder=4)
            if replicate[1]["category"] == "evolved":
                ax.scatter(x[j] + width,
                           y = replicate[1]["value"],
                           s=10,
                           facecolors="none",
                           edgecolors="black",
                           linewidths=1.0,
                           zorder=4)
        j = j+1
    
    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('labeled fraction')
    ax.set_xticks(x)
    ax.set_xticklabels(metabolite_list, rotation = "vertical",ha="center")
    plt.legend(loc="upper left",frameon=False)
    plt.yticks([0.0,0.2,0.4,0.6,0.8,1.0])
    plt.ylim(0.0, 1.1)
    plt.tight_layout()
    plt.savefig("fractional_labeling_" + strain + ".pdf")
    

metabolite_list = []
strain_list = []

for metabolite in data["metabolite"]:
    if metabolite not in metabolite_list:
        metabolite_list.append(metabolite)
        
for strain in data["strain"]:
    if strain not in strain_list:
        strain_list.append(strain)
        
for strain in strain_list:
    plot_fractional_labeling(data[data["strain"] == strain], data_replicates[data_replicates["strain"] == strain], metabolite_list, strain)
