# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 09:34:07 2020

@author: phili
"""


import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['font.family'] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

#Supplementary Figure 6b, methanol-dependent growth of evolved DfrmADfbp on methanol and pyruvate

palette = [("#E0A6C0"),
           ("#E0A6C0"),
           ("#E0A6C0"),
           ("#E0A6C0"),
           ("#E0A6C0"),
           ("#2D2E83"),
           ("#E84344"),
           ("#A14A96"),
           ("#A14A96"),
           ("#A14A96"),
           ("#A14A96"),
           ("#A14A96"),
          ]

#load dataset of evolved DfrmADfbp from textfile
data = pd.read_csv("20200601_fbp_evolved_growthphenotype_shakeflask.txt", header=None, delimiter="\t")
#load dataset of ancestral DfrmADfbp from textfile
data_ancestral = pd.read_csv("20200629_fbp_ancestral_growthphenotype_shakeflask.txt", header=None, delimiter="\t")

#transpose data as a first step to later fit to seaborn format
data = data.T
#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}
#change first row to header row and set all values to float format
header = data.iloc[0]
data = data[1:]
data = data.astype(float)
data = data.rename(columns = header)

#reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
data = data.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")

#transpose data as a first step to later fit to seaborn format
data_ancestral = data_ancestral.T
#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}
#change first row to header row and set all values to float format
header = data_ancestral.iloc[0]
data_ancestral = data_ancestral[1:]
data_ancestral = data_ancestral.astype(float)
data_ancestral = data_ancestral.rename(columns = header)

#reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
data_ancestral = data_ancestral.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")

data = data_ancestral.append(data)

#--plot--------------------------------------------------------------------------------------------------------------------
plt.figure(figsize=(2.75,2.5))

fig = sns.lineplot(x='Time [h]', y='$\mathregular{OD_{600}}$', hue='Conditions',
                   data=data,
                   palette=palette,
                   marker="o",
                   markersize=8,
                   markeredgewidth=0.5,
                   markeredgecolor="None",
                   lw=2,
                   ci="sd",
                   err_style='bars',
                   err_kws = err_kws,
                   legend = False
                   )

fig.set(xlim=(0, None),ylim=(0,1.6))

fig.set_xlabel(xlabel = "time [h]")

#save the figure
fig.figure.savefig("supplementary_fig_6b_evolved_and_ancestral_fbp" + ".pdf", bbox_inches="tight", dpi=1200)

#Supplementary Figure 6c, methanol-dependent growth of evolved DfrmADtpiA on methanol and pyruvate

palette = [("#E0A6C0"),
           ("#E0A6C0"),
           ("#E0A6C0"),
           ("#E0A6C0"),
           ("#2D2E83"),
           ("#E84344"),
           ("#A14A96"),
           ("#A14A96"),
           ("#A14A96"),
           ("#A14A96")
          ]

#load dataset of evolved DfrmADfbp from textfile
data = pd.read_csv("20200615_tpiA_evolved_growthphenotype_shakeflask.txt", header=None, delimiter="\t")
#load dataset of ancestral DfrmADfbp from textfile
data_ancestral = pd.read_csv("20200629_tpiA_ancestral_growthphenotype_shakeflask.txt", header=None, delimiter="\t")

#transpose data as a first step to later fit to seaborn format
data = data.T
#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}
#change first row to header row and set all values to float format
header = data.iloc[0]
data = data[1:]
data = data.astype(float)
data = data.rename(columns = header)

#reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
data = data.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")

#transpose data as a first step to later fit to seaborn format
data_ancestral = data_ancestral.T
#define the size of the caps of the error band
err_kws = {"capsize":4,
          "capthick":0.5}
#change first row to header row and set all values to float format
header = data_ancestral.iloc[0]
data_ancestral = data_ancestral[1:]
data_ancestral = data_ancestral.astype(float)
data_ancestral = data_ancestral.rename(columns = header)

#reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
data_ancestral = data_ancestral.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")

data = data_ancestral.append(data)

#--plot--------------------------------------------------------------------------------------------------------------------
plt.figure(figsize=(2.75,2.5))

fig = sns.lineplot(x='Time [h]', y='$\mathregular{OD_{600}}$', hue='Conditions',
                   data=data,
                   palette=palette,
                   marker="o",
                   markersize=8,
                   markeredgewidth=0.5,
                   markeredgecolor="None",
                   lw=2,
                   ci="sd",
                   err_style='bars',
                   err_kws = err_kws,
                   legend = False
                   )

fig.set(xlim=(0, None),ylim=(0,1.6))

fig.set_xlabel(xlabel = "time [h]")

#save the figure
fig.figure.savefig("supplementary_fig_6c_evolved_and_ancestral_tpiA" + ".pdf", bbox_inches="tight", dpi=1200)