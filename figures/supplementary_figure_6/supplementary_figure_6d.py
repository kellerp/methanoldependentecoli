# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 09:34:34 2020

@author: phili
"""
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress

plt.rcParams['font.family'] = "arial"
plt.rcParams['pdf.fonttype'] = 42
plt.rcParams['ps.fonttype'] = 42

def rearrange_data(data):
    #transpose data as a first step to later fit to seaborn format
    data = data.T
    #change first row to header row and set all values to float format
    header = data.iloc[0]
    data = data[1:]
    data = data.astype(float)
    data = data.rename(columns = header)

    #reorder data to fit to seaborn format (each OD measurement needs an assigned sampling time and sample name)
    data = data.melt('Time [h]', var_name='Conditions', value_name="$\mathregular{OD_{600}}$")
    
    return data


def calculate_growth_rate(strain, time_range, data):
    data["ln_value"] = np.log(data["$\mathregular{OD_{600}}$"])

    x = []
    y = []
    
    if type(strain) != float:
        for index, row in data.iterrows():
            if type(row.Conditions) != float:
                if strain in row.Conditions:
                    if (row["Time [h]"] > time_range[0]) & (row["Time [h]"] < time_range[1]):
                        x.append(row["Time [h]"])
                        y.append(row.ln_value)
    
    if x != []:
        linregress_result = linregress(x, y)
        growth_rate = float(linregress_result[0])
        r_value = float(linregress_result[2])
        return growth_rate, r_value
    else:
        return float("nan"), float("nan")

def calculate_average_growth_rate(strains, time_range_lower_limit, time_range_upper_limit, data):
    growth_rates = []
    r_values = []
    
    time_range = [time_range_lower_limit, time_range_upper_limit]
    
    for strain in strains:
        growth_rate, r_value = calculate_growth_rate(strain, time_range, data)
        growth_rates.append(growth_rate)
        r_values.append(r_value) 
    
    if growth_rates != []:
        return np.nanmean(growth_rates), np.nanstd(growth_rates), np.nanmean(r_values), np.nanstd(r_values), growth_rates
    else:
        return float("nan")

def calculate_average_growth_rates(paths, strains, time_ranges_lower_limit, time_ranges_upper_limit):
    paths_and_time_ranges = pd.DataFrame(index = [paths], columns = ["average growth rate", "standard deviation growth rate", "average r value", "standard deviation r value", "growth rates"])
    paths_and_time_ranges["time range lower limit"] = time_ranges_lower_limit
    paths_and_time_ranges["time range upper limit"] = time_ranges_upper_limit

    for path in paths_and_time_ranges.index:
        data = pd.read_csv(path[0], header=None, delimiter="\t")
        data = rearrange_data(data)
        paths_and_time_ranges.at[path, "average growth rate"],\
        paths_and_time_ranges.at[path, "standard deviation growth rate"],\
        paths_and_time_ranges.at[path, "average r value"],\
        paths_and_time_ranges.at[path, "standard deviation r value"],\
        paths_and_time_ranges.at[path, "growth rates"]\
        = calculate_average_growth_rate(
            strains,
            paths_and_time_ranges.loc[path]["time range lower limit"],
            paths_and_time_ranges.loc[path]["time range upper limit"],
            data)
    return paths_and_time_ranges

def calculate_doubling_time(df):
    df["average doubling time"] = np.log(2)/(df["average growth rate"])
    df["standard deviation doubling time"] = (df["standard deviation growth rate"]/df["average growth rate"])*df["average doubling time"]
    return df
        
paths = ["20200629_fbp_ancestral_growthphenotype_shakeflask.txt",
         "20200601_fbp_evolved_growthphenotype_shakeflask.txt",
         "20200629_tpiA_ancestral_growthphenotype_shakeflask.txt",
         "20200615_tpiA_evolved_growthphenotype_shakeflask.txt"]

strains = ["MeOH/Pyr R1", "MeOH/Pyr R2", "MeOH/Pyr R3", "MeOH/Pyr R4", "MeOH/Pyr R5"]
strain_list = ["fbp ancestral", "fbp evolved", "tpiA ancestral", "tpiA evolved"]

time_ranges_lower_limit = [35,19,35,18]
time_ranges_upper_limit = [45,30,50,38]

results = calculate_average_growth_rates(paths, strains, time_ranges_lower_limit, time_ranges_upper_limit)

results = calculate_doubling_time(results)

plt.figure(figsize=(2.75,2.5))

plt.bar(x = strain_list,
            height = "average growth rate",
            yerr = "standard deviation growth rate",
            capsize=3.0,
            color=["#E0A6C0","#A14A96","#E0A6C0","#A14A96"],
            data = results)

plt.ylim([0,0.2])
plt.xticks(["","","",""])

i=0
for i in range(len(paths)):
    for value in results.loc[paths[i], "growth rates"].values[0]:
        plt.scatter(x = strain_list[i],
                y = value,
                s=10,
                facecolors="none",
                edgecolors="black",
                linewidths=1.0,
                zorder=4)
    i = i + 1

plt.tight_layout()

plt.savefig("supplementary_fig_6d_growth_rates" + ".pdf", bbox_inches="tight", dpi=1200)